package d20161012;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

public class N4 {
	// 여러 개의 자연수를 입력 받은 후, 그 수들을 이어붙여서 만들 수 있는
	// 가장 큰 수와 가장 작은 수의 합을 구하기
	public static void main(String[] args) throws IOException {
		// 사용자로부터 입력받기
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		// 입력받은 값을 띄어쓰기를 기준으로 나누기
		StringTokenizer st = new StringTokenizer(bf.readLine());
		
		// StirngTokenizer로 나눈 값을 list에 하나씩 넣어주기
		// 2자리 정수와 1자리 정수를 나눠서 배열에 저장
		ArrayList<Integer> list = new ArrayList<Integer>();

		while(st.hasMoreTokens()){ 
			list.add(Integer.valueOf(st.nextToken()));
		} // while end
		
		Collections.sort(list); // list정렬
		
		// 한 자리 숫자들을 중간에 넣기
		int idx = 0;
		int idx2 = 1;
		while(list.get(idx)/10 == 0){ // 한 자리 숫자일동안만!
			int num = list.get(idx); // 한 자리의 숫자가 저장되어있음
			for(int i = idx2; ;i++){
				if((num>=list.get(i)/10) && (num<=list.get(i+1)/10) && (num>=list.get(i)%10) && (num<=list.get(i+1)%10)){
					// num <= list.get(i)%10
					list.add(i, num);
					list.remove(idx);
					break;
				} // if end
				idx2++;
			} // for end
			idx++; // while문 끝내기 위해?
		} // while end

		
	} // main end
}// class end
