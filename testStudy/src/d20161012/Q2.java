package d20161012;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Q2 {
	public static void main(String[] args) throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		/*String[] strs = bf.readLine().split(","); // 한 줄 읽기
		int sum = 0;
		for(String x : strs){
			sum += Integer.valueOf(x);
		} // for end
		System.out.println(sum);*/
		
		String line = bf.readLine();
		
		
		StringTokenizer st = new StringTokenizer(line, ",");
		int sum = 0;
		while(st.hasMoreTokens()){
			sum += Integer.valueOf(st.nextToken());
		}
		System.out.println(sum);
		
	}// main end
}// class end
