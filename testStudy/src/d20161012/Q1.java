package d20161012;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Q1 {
	public static void main(String[] args) throws IOException {
		// 입력 받은 수들의 합 구하기
		// Scanner보다 BufferReader가 입력받을 때 빠르다.
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(bf.readLine());
		int sum = 0;
		while(st.hasMoreTokens()){
			sum += Integer.valueOf(st.nextToken());
		} // while end
		
		System.out.println(sum);
	}// main end
}// class end
