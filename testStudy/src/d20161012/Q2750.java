package d20161012;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Q2750 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		ArrayList<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < size; i++){
			int num = sc.nextInt();
			list.add(num);
		}
		
		Collections.sort(list);
		
		for(Integer x : list){
			System.out.println(x);
		}
	} // main end

} // class end
