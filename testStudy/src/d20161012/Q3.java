package d20161012;

import java.util.Scanner;

public class Q3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		
		StringBuilder sb = new StringBuilder();
		for(int i = 1; i <= num; i++){
			sb.append(i+"\n");
		}// for end
		
		System.out.println(sb);
	} // main end

} // class end
